# OnePCBMultiBOM

#### 介绍
一个PCB有多个BOM的情况下，根据规则自动生成BOM的工具。


#### 使用说明
- 首先需要有完整的BOM文件
- CCR（交叉参考）文件是指所有元器件按照原理图进行分类的文件
- 编辑需要删除的元器件的脚本
- 运行程序根据脚本自动生成多个BOM文件



```
Usage:
    python ./OnePCBMultiBOM.py
        -f 命令行脚本文件

        --iuput "BOM.xlsx"
        --output "OUT.xlsx"

        // 第一种CCR匹配方式，ccr_match_del 可以多次出现，推荐使用
        --ccr_doc "CCR.xlsx"
        --ccr_match_del "sch1.SchDoc"
        --ccr_match_del "sch2.SchDoc#_A,_B,_C,_D"
        --ccr_match_del "sch3.SchDoc#_A,_C"

        // 第二种CCR匹配方式，此种方式适合有多个 ccr 文档使用，一般 ccr 文档只有一个
        --ccr "ccr.xlsx@sch1.SchDoc#_A,_B,_C,_D|sch2.SchDoc|sch3.SchDoc#_A,_C"

        // 后缀匹配，component_match_del 可以多次出现
        --component_match_del "R1,R2,R3"
        --component_match_del "R27,R28#_A,_B"
        --component_match_del "R29#_A,_B"

        // 从最终输出的BOM中删除数量为 0 的元器件
        --delete

        --version
```

