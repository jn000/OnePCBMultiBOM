# coding utf-8
'''
Usage:
    python ./OnePCBMultiBOM.py
        -f 命令行脚本文件

        --iuput "input_file"
        --output "output_file"

        // 第一种CCR匹配方式，此种方式适合有多个 ccr 文档使用，一般 ccr 文档只有一个
        --ccr_remove "ccr.xlsx@sch1.SchDoc#_A,_B,_C,_D|sch2.SchDoc|sch3.SchDoc#_A,_C"

        // 第二种CCR匹配方式，ccr_match 可以多次出现，推荐使用
        --ccr_doc "ccr.xlsx"
        --ccr_match "sch1.SchDoc#_A,_B,_C,_D"
        --ccr_match "sch2.SchDoc"
        --ccr_match "sch3.SchDoc#_A,_C"

        // 后缀匹配，component_match 可以多次出现
        --component_match "R1,R2,R3"
        --component_match "R28#_A,_B"
        --component_match "R29#_A,_B"

        // 从最终输出的BOM中删除数量为 0 的元器件
        --delete

        --version
'''

import openpyxl
import argparse
from pathlib import Path
import os

description = '''
同一个PCB线路板，不同功能，生成不同的BOM清单
'''

epilog = '''
开发新产品过程中，同一个电路板会通过贴装不同的元器件来实现不同的功能，此时需要生成不同的BOM清单，此工具
通过 模式匹配 和 全字匹配 可以帮助生成BOM清单，避免工程师通过excel一个一个删除元器件，只需提供

- 完整的BOM清单
- 可选的交叉元件参考文件（CCR），可以通过Altium Designer软件来生成
- 命令行参数模板

即可运行

                                        nikoladi@163.com'''

# 文档分隔符
CCR_DOC_SPLIT = '@'
# 原理图组分隔符
CCR_SCH_GROUP_SPLIT = '|'
# 原理图与匹配分隔符
CCR_SCH_MATCH_SPLIT = '#'

# 元件与匹配分隔符
COMPONENT_MATCH_SPLIT = '#'
# 匹配之间的分隔符
MATCH_SPLIT = ','
# BOM中的位号的分隔符
BOM_NAME_SPLIT = ','


def excel_read(file_name):
    content = []
    wb = openpyxl.load_workbook(file_name)

    sheets_name = wb.sheetnames
    sheet = wb[sheets_name[0]]

    rows = sheet.rows
    for i, cells in enumerate(rows):
        line = []
        for j, cell in enumerate(cells):
            line.append(cell.value)
        content.append(line)
    wb.close()
    return content


def excel_write(file_name, content):
    try:
        p = Path(file_name)
        p.parent.mkdir()
    except FileExistsError:
        # print('BOM文件夹已经存在')
        pass

    wb = openpyxl.Workbook()

    ws = wb.active
    ws.title = p.stem

    for x in content:
        ws.append(x)

    wb.save(file_name)
    wb.close()
    pass


def bom_dispose(content, match_set, is_delete):
    names_num = ' '
    quantity_num = ' '
    result = []
    for i, title in enumerate(content[0]):
        # print('{}'.format(title), end=' ')
        if title == '位号' or title == "Designator":
            names_num = i
        elif title == '数量' or title == "Quantity":
            quantity_num = i
    # print('')
    names = []
    if names_num == ' ' or quantity_num == ' ':
        print('BOM 文档字段 位号（Designator） 或 数量（Quantity） 不存在，请检查后运行...')
        exit(1)
    result.append(content[0])
    for j, row in enumerate(content[1:]):
        if row[names_num] is not None:
            names = [x.strip() for x in row[names_num].split(BOM_NAME_SPLIT)]
            # print(names)
            if len(names) != row[quantity_num]:
                print('BOM 文档第 {} 行数量错误,计算数量/文档数量 {}/{}，请检查后运行...'.format(
                    j + 2, len(names), row[quantity_num]))
                exit(1)
            row[names_num] = set(names)

            # TODO
            # old_len = len(row[names_num])
            row[names_num] -= match_set
            # new_len = len(row[names_num])
            # print('{1}/{2} {0}'.format(row[1], old_len, new_len))

            row[quantity_num] = len(row[names_num])
            row[names_num] = BOM_NAME_SPLIT.join(row[names_num])
            # print(row)
            if is_delete is True:
                if row[quantity_num] != 0:
                    result.append(row)
            else:
                result.append(row)
        else:
            result.append(row)
    return result


def ccr_dispose(content, sch_group):
    names_num = ' '
    quantity_num = ' '
    document_num = ' '
    match_set = set()
    match_list = []

    for i, title in enumerate(content[0]):
        # print('{}'.format(title), end=' ')
        if title == '文档' or title == "Document":
            document_num = i
        elif title == '位号' or title == "Designator":
            names_num = i
        elif title == '数量' or title == "Quantity":
            quantity_num = i
    # print('')
    # print('document:{} names:{} quantity:{}'.format(document_num, names_num,
    #                                                 quantity_num))

    if document_num == ' ' or names_num == ' ' or quantity_num == ' ':
        print(
            'CCR 文档字段 文档（Document） 或 位号（Designator） 或 数量（Quantity） 不存在，请检查后运行...'
        )
        exit(1)

    for group in sch_group:
        doc_match = ' '
        s_m = group.split(CCR_SCH_MATCH_SPLIT)
        doc_name = s_m[0]
        if len(s_m) >= 2:
            doc_match = s_m[1].split(MATCH_SPLIT)

        # print('doc:{} match:{}'.format(doc_name, doc_match))
        valid = False
        for j, lines in enumerate(content[1:]):
            p = Path(lines[document_num])
            if p.name == doc_name:
                match_components = []
                valid = True
                s = [x.strip() for x in lines[names_num].split(BOM_NAME_SPLIT)]
                if len(s) != lines[quantity_num]:
                    print('CCR 文档第 {} 行数量错误,计算数量/文档数量 {}/{}，请检查后运行...'.format(
                        j + 2, len(s), lines[quantity_num]))
                    exit(1)
                if doc_match != ' ':
                    # 后缀匹配
                    for ss in doc_match:
                        z_list = [x for x in s if x[0 - len(ss):] == ss]
                        # print(z_list)
                        match_components += z_list

                else:
                    # 文档内所有元件全部匹配
                    match_components = s
                match_list += match_components
                print('{} 匹配了 {} 个元器件'.format(doc_name, len(match_components)))
        if valid is False:
            print('CCR 文档中没有匹配的原理图 {} ，请核实后运行...'.format(doc_name))
            exit(1)
    match_set = set(match_list)
    print('CCR 共匹配 {} 个元器件'.format(len(match_set)))
    print('')
    return match_set


def script_file_dispose(args):
    ccr = args.ccr
    input_file = args.input
    output_file = args.output
    component = args.component
    component_match_list = args.component_match
    ccr_doc = args.ccr_doc
    ccr_match = args.ccr_match
    bom_delete = args.delete

    match_set = set()

    # print("请仔细检查BOM清单，确保格式正确...")

    # 获取ccr文件名，并读取内容
    if ccr is not None and CCR_DOC_SPLIT in ccr:
        param_list = ccr.split(CCR_DOC_SPLIT)
        ccr_doc_name = param_list[0]
        ccr_sch_group = param_list[1].split(CCR_SCH_GROUP_SPLIT)
    elif ccr_doc is not None and ccr_match is not None:
        ccr_doc_name = ccr_doc
        ccr_sch_group = ccr_match
        # print(ccr_doc_name)
        # print(ccr_sch_group)
    else:
        ccr_doc_name = None

    if ccr_doc_name is not None:
        ccr_content = excel_read(ccr_doc_name)

        # 从ccr文件中根据命令行字符串获取匹配的元件集合
        match_set = ccr_dispose(ccr_content, ccr_sch_group)

    comp_set = set()
    if component is not None:
        match_components = component.split(MATCH_SPLIT)
        match_set |= set(match_components)
        print('全字匹配（单次） {} 个元器件'.format(len(match_components)))

    # print(component_match_list)
    if component_match_list is not None:
        for c_m_str in component_match_list:
            tmp_set = set()
            c_ms = c_m_str.split(COMPONENT_MATCH_SPLIT)
            if len(c_ms) == 1:
                match_components = c_ms[0].split(MATCH_SPLIT)
                tmp_set = set(match_components)
                print('全字匹配（追加） {} 个元器件'.format(len(tmp_set)))
                match_set |= tmp_set
            else:
                match_components = c_ms[0].split(MATCH_SPLIT)
                match_sufs = c_ms[1].split(MATCH_SPLIT)
                for x in match_components:
                    for y in match_sufs:
                        tmp_set.add(x + y)
                print('后缀匹配（追加） {} 个元器件'.format(len(tmp_set)))
                match_set |= tmp_set
            comp_set |= tmp_set
            # print(tmp_set)
        print('全字/后缀共匹配 {} 个元器件'.format(len(comp_set)))
        print()
    print('总共匹配 {} 个元器件，即将从BOM表中删除这些元器件'.format(len(match_set)))

    # 读取原始BOM清单，从清单中删除元件集合
    row_bom_content = excel_read(input_file)
    new_bom_content = bom_dispose(row_bom_content, match_set, bom_delete)

    excel_write(output_file, new_bom_content)


def main():
    parser = argparse.ArgumentParser(
        prog='OnePCBMultiBOM',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=description,
        epilog=epilog,
        fromfile_prefix_chars='@')
    parser.add_argument('-f',
                        '--script_file',
                        action='append',
                        dest='script_files',
                        help='命令行脚本文件')
    parser.add_argument('-i', '--input', dest='input', help='输入BOM文件名')
    parser.add_argument('-o', '--output', dest='output', help='输出BOM文件名')
    parser.add_argument('--ccr_doc', help='输入CCR文件名')
    parser.add_argument('--ccr_match_del',
                        dest='ccr_match',
                        help='''删除CCR文件中匹配的元器件，可以多次出现
        --ccr_match_del ble.SchDoc 删除 ble.SchDoc 文档中的所有元器件
        --ccr_match_del usb.SchDoc#_A,_B  删除 usb.SchDoc 文档中以 _A _B 结尾的元器件''',
                        action='append')
    parser.add_argument('--component_match_del',
                        dest='component_match',
                        action='append',
                        help='''删除指定的和匹配的元器件，可以多次出现
                        --component_match_del R1,R2,R3 删除匹配的元器件
                        --component_match_del R28#_A,_B 删除后缀匹配的元器件
                        ''')
    parser.add_argument('-d',
                        '--delete',
                        help='删除输出文件中数量为0的元器件',
                        action="store_true")
    parser.add_argument('-c',
                        '--component_del',
                        dest='component',
                        help='删除指定的元器件')
    parser.add_argument('--ccr',
                        help='''交叉元件参考(Cross Component Reference)配置
        --ccr
        "ccr.xlsx@sch1.SchDoc#_A,_B,_C,_D|sch2.SchDoc|sch3.SchDoc#_A,_C"''')
    parser.add_argument('-v',
                        '--version',
                        action='version',
                        version='%(prog)s v1.0')
    args = parser.parse_args()

    script_files = args.script_files

    # 从文件中获取命令行参数
    for x in script_files:
        args = parser.parse_args(['@{}'.format(x)])
        pp = Path(x)
        # print(pp.absolute().parent)
        os.chdir(pp.absolute().parent)

        script_files = args.script_files
        if script_files is not None:
            for x in script_files:
                args = parser.parse_args(['@{}'.format(x)])
                print('\n{:=^60}'.format(x))
                script_file_dispose(args)
        else:
            print('\n{:=^60}'.format(x))
            script_file_dispose(args)

    print('\n恭喜，执行成功，Congratulations!!!')


if __name__ == "__main__":
    main()
